import debug                        from 'debug';
import e2                           from 'eventemitter2';
import {Message, Request, Response} from './message';
import {TransactionManager as TM}   from './transaction';
import {DialogManager as DM}        from './dialog';
import {Registrant}                 from './registrant';
import {DEFAULT, ALL, RESPONSE}     from './common';


export function attach_to_ws(peer, ws) {
    peer.tx = m => {
        let d = JSON.stringify(m);
        try {
            ws.send(d);
        } catch(e) {
            peer.dbg(e);
        }
    };

    const open = () => peer.registrant && peer.registrant.start();
    ws.on('open', open);

    const recv = e => {
        try {
            peer.rx(Message.parse(e.data));
        } catch(e) {
            peer.dbg(e);
        }
    };
    ws.on('message', recv);

    peer.detach = async function() {
        this.dbg('Detaching peer');

        if (this.registrant) {
            await this.registrant.stop();
        }

        await Promise.all([this.tm.stop(), this.dm.stop()]);

        // FIXME: stop event listeners for tm and dm

        ws.removeListener('message', recv);
        ws.removeListener('open', open);
        this.tx = () => undefined;
        delete this.detach;
        this.dbg('Peer detached');
    };
}

export class Peer extends e2.EventEmitter2 {
    constructor(uri, opts) {
        super({
            newListener: true
        });

        // FIXME: parse the URI into NodeURI here (will make proxy fail)
        this.uri = uri;

        if (opts) this.opts = {...opts};
        else this.opts = {};

        let id = this.uri.node.toString().slice(0,6);
        this.dbg = debug(`sece:peer[${id}]`);

        // Create a dummy transmit function until we're connected. Objects
        // overriding the peer are supposed to change the function once
        // connected/attached.
        this.tx = _m => undefined;

        this.tm = new TM();
        this.sendf = (...args) => this.tx(...args);
        this.T = r => this.tm.createTransaction(r, this.sendf);

        this.dm = new DM();
        this.D = (...args) => this.dm.createDialog(...args);

        // Resubmit all events generated by transaction and dialog managers on
        // this object
        this.tm.onAny((n, e) => this.emit(`tm_${n}`, e));
        this.dm.onAny((n, e) => this.emit(`dm_${n}`, e));

        this.rx = this.rx.bind(this);

        if (opts.registrar_uri) {
            this.registrant = new Registrant(this, opts.registrar_uri, opts);
            this.registrant.onAny(e => this.emit(e));
        }

        this.on('newListener', (e, f) => {
            if (e === 'registered'   && this.registered)  f();
            if (e === 'unregistered' && !this.registered) f();
        });
    }

    createRequest(to) {
        let r = new Request(to, this.uri);
        r.addVia(this.uri);

        if (this.opts.proxy_uri)
            r.addRoute(this.opts.proxy_uri);

        return r;
    }

    get registered() {
        return this.registrant ? this.registrant.registered : false;
    }

    rx(m) {
        // If this is a response, first check that it belongs to us,
        // according to the topmost via.
        if (m.isResponse) {
            if (!m.via || !Array.isArray(m.via) || m.via.length != 1) {
                this.dbg('Wrong via, dropping');
                // FIXME: emit an event here
                return;
            }
        }

        // Now see if the message can be matched to an existing
        // transaction. If yes, pass the message, to the transaction.
        if (this.tm.rx(m)) return;

        // If we receive a response that was not matched by the
        // transaction layer, emit an event on the peer object in case
        // somebody is listening for such responses.
        if (!m.isRequest) {
            this.emit(RESPONSE, m);
            return;
        }

        if (this.opts.checkDestination) {
            if (m.next.toString() !== this.uri.toString()) return;
        }

        // No existing transaction for the request was found. Now try to
        // see if the request belongs to an existing dialog.
        if (this.dm.rx(m, this)) return;

        // No dialog found, see if we can emit the request "statelessly"
        // on the peer itself.

        let listeners =
        this.listenerCount(m.method)
        + this.listenerCount(DEFAULT)
        + this.listenerCount(ALL);

        if (listeners > 0) {
            // No transaction or dialog was matched for the request. Let the
            // peer object emit events in case there is anybody listening
            // for such requests.
            this.emit(m.method, m, this.T, this.D, this.sendf);

            if (this.listenerCount(m.method) == 0)
                this.emit(DEFAULT, m, this.T, this.D, this.sendf);

            this.emit(ALL, m, this.T, this.D, this.sendf);
        } else {
            // No listeners registered, send a negative response back to
            // indicate that there is nobody to handle the request.
            this.sendf(Response.fromRequest(m, 503, 'Service Unavailable'));
        }
    }
}
