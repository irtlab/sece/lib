import e2         from 'eventemitter2';
import {Response} from './message';
import SHA1       from 'crypto-js/sha1';
import {
    TRANSACTION_TIMEOUT,
    TRANSACTION_DELETE_TIMEOUT,
    RETRANSMIT_TIMEOUT_MIN,
    RETRANSMIT_TIMEOUT_MAX,
    RETRANSMIT_PROVISIONAL
}              from './common';
import debug   from 'debug';
import {GUID}  from './guid';

const dbg = debug('sece:T');

export function TransactionID(via)
{
    if (typeof via !== 'undefined') {
        if (!via.uri || !via.tid) throw new Error('Invalid Via header');
        return SHA1(`${via.uri}/${via.tid}`).toString();
    } else {
        return GUID();
    }
}


export class TransactionManager extends e2.EventEmitter2 {
    constructor() {
        super();
        this.transactions = {};
    }

    async stop() {
        dbg('Stopping transaction manager');
        await Promise.all(Object.values(this.transactions).map(async (t) => {
            try {
                await t.stop();
            } catch(e) {
                t.dbg(e);
            }
        }));
        this.transactions = {};
        dbg('Transaction manager stopped');
    }

    createTransaction(request, tx) {
        let t = new Transaction(request, tx),
            key = t.id.toString();

        this.transactions[key] = t;
        let cleanup = () => {
            t.dbg(`Completed, starting delete timer`);
            setTimeout(() => {
                t.dbg(`Deleted`);
                delete this.transactions[key];
                this.emit('delete', t);
            }, TRANSACTION_DELETE_TIMEOUT);
        };

        t.resolved.then(cleanup, e => {
            cleanup();
            throw e;
        });

        this.emit('create', t);
        return t;
    }

    rx(m) {
        if (!m.via || !m.via[0] || !m.via[0].tid) return null;

        let tid;
        if (m.isRequest) tid = TransactionID(m.via[0]);
        else tid = m.via[0].tid;

        let t = this.transactions[tid.toString()];
        if (typeof t === 'undefined') {
            return null;
        } else {
            if (t.rx(m)) return t;
            else return null;
        }
    }
}


/**
 * Implements a signaling transaction.
 *
 * The signaling transaction is the basic unit of interaction in the
 * signaling protocol. A transaction consists of a request message and
 * one or more response messages related to the request. Each
 * transaction is uniquely identified by a randomly generated
 * identifiers. The identifier is carried a header of all the requests
 * and responses that belong to the transaction.
 *
 * A transaction is started when a client creates a new transaction
 * object given a request message and invokes the send method on the
 * transaction. From that moment on, the transaction keeps
 * retransmitting the request until a final response has been received
 * from the remote party. If no final response is received within a
 * certain amount of time, the transaction times out.
 *
 * Each transaction contains a promise that is resolved when a final
 * response has been received and rejected if the transaction timeout
 * out. The transaction object provides a promise-like API with then
 * and catch methods. Note: A resolved transaction does not indicate
 * success, only that some final response has been received from the
 * remote party.
 *
 * A server may send one or more provisional responses before it sends
 * a final response. There can be only one final response. The final
 * response is guaranteed to be delivered reliably. This is guaranteed
 * by the client transaction retransmitting the request until a final
 * response has been received. Provisional responses are NOT
 * guaranteed to be delivered reliably and in order.
 *
 * Each transaction contains a timer that fires if no final response
 * has been sent on the transaction within some pre-configured amount
 * of time. Provisional responses allow disabling this timer on the
 * server. By sending a provisional response on a transaction, the
 * server indicates that it is working on genenerating the final
 * response and the transaction timer no longer applies, as long as
 * the client keeps retransmitting the request. If the client stops
 * retransmitting the request, the transaction times out anyway (to
 * ensure garbage collection). On each request retransmission the
 * server transaction re-sends the most recent provisional response.
 * Provisional responses make it possible to keep a transaction alive
 * indefinetly, provided that the client keeps retransmitting the
 * request to indicate pending interest. This is useful for long-lived
 * requests.
 *
 * Events:
 *   - transmit(str)
 *       Emited each time a transaction trasmits a message
 *   - timeout(error)
 *       Emitted once when the transaction has timed out
 *   - resolve(response)
 *       Emitted once when the transaction has been resolved
 *   - provisional(response)
 *       Provisional response has been sent or received
 *
 * TODO:
 *   - Implement acknowledged final responses, where the server
 *     transaction does not resolve until the receipt of the final
 *     response has been acknowledged by the client.
 *   - Implement a better way to generate transaction identifiers so
 *     that a request can spiral through proxies.
 *
 */
export class Transaction extends e2.EventEmitter2 {
    constructor(request, tx) {
        super();

        this.request = request;
        this.tx = tx;

        request.via = request.via || [];
        request.via[0] = request.via[0] || {};

        if (request.via[0].tid) {
        // FIXME: This only works if we are target of the transaction.
        // It won't work, e.g., on a transaction forwarding proxy. We'd
        // need to save the id in the request there.
            this.id = TransactionID(request.via[0]);
        } else {
            this.id = TransactionID();
            request.via[0].tid = this.id;
        }

        this.timers = {};
        this._running = false;

        this.resolved = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });

        let id = this.id.slice(0, 6);
        this.dbg = debug(`sece:T[${id}]`);
    }

    _send(data) {
        try {
            this.dbg(`Transmitting ${data.method}`);
            this.tx(data);
            this.emit('transmit', data);
        } catch(e) {
            console.log(e);
        }
    }

    _retransmit(tout) {
        this._send(this.request);
        this._resetRetransmissions(tout);
    }

    _resetRetransmissions(tout) {
        // Keep retransmitting the request with an exponential backoff
        // until the maximum interval is reached. Then keep retransmitting
        // with the maximum interval until the transaction rejects or
        // resolves.

        if (this.timers.retransmit) {
            clearTimeout(this.timers.retransmit);
            delete this.timers.retransmit;
        }

        this.timers.retransmit = setTimeout(() => {
            let next = tout * 2;
            if (next > RETRANSMIT_TIMEOUT_MAX)
                next = RETRANSMIT_TIMEOUT_MAX;
            this._retransmit(next);
        }, tout);
    }

    _resetTimeout() {
        if (this.timers.timeout) {
            clearTimeout(this.timers.timeout);
            delete this.timers.timeout;
        }

        this.timers.timeout = setTimeout(() => { void this.stop(); }, TRANSACTION_TIMEOUT);
    }

    _stopAllTimers() {
        Object.values(this.timers).forEach(t => clearTimeout(t));
        this.timers = {};
    }

    /**
     * Start re-transmitting the request until we receive a final
     * response or time out. This is the main method for client
     * transactions.
     */
    start() {
        this.dbg('Starting transaction');
        this._running = true;
        this._client = true;
        this._retransmit(RETRANSMIT_TIMEOUT_MIN);
        this._resetTimeout();
        return this;
    }

    stop() {
        // FIXME: Add support for cancel
        if (!this._running) return;
        this._running = false;
        this._stopAllTimers();
        let e = new Error('Transaction Timeout');
        this._reject(e);
        setTimeout(() => this.emit('timeout', e));
        this.dbg('Timed out');
    }

    _sendResponse(response) {
        this.response = response;
        this._send(response);
    }

    _createResponse(...args) {
        if (arguments.length !== 1 || typeof arguments[0] !== 'object')
            return Response.fromRequest(this.request, ...args);
        else
            return arguments[0];
    }

    send(...args) {
        let response = this._createResponse(...args);
        this._sendResponse(response);

        if (response.code < 200) {
            setTimeout(() => this.emit('provisional', response));
            this.dbg(`Sending provisional response ${response.code}`);
        } else {
            this.dbg(`Resolved with ${response.code}`);
            this._resolve(response);
            setTimeout(() => this.emit('resolve', response));
        }
        return this;
    }

    _rx_client(msg) {
        if (msg.isRequest) return false;

        // If we already have the final response, ignore the message.
        if (this.response) return true;

        if (msg.code < 200) {
        // On each provisional response received, reset the
        // transaction's timeout. That way, the transaction's lifetime
        // can be kept indefinitely, as long as the server keeps sending
        // provisional reponses.
            this._resetTimeout();

            // Once we have received at least one provisional response,
            // there is no need to retransmit with an exponential timer
            // anymore. We do, however, keep retransmitting to notify the
            // server that we are still interested in the transaction.
            this._resetRetransmissions(RETRANSMIT_PROVISIONAL);

            this.emit('provisional', msg);
            this.dbg(`Received provisional response ${msg.code}`);
        } else {
            this.response = msg;
            this._running = false;
            this._stopAllTimers();
            this._resolve(msg);
            this.emit('resolve', msg);
            this.dbg(`Resolved with ${msg.code}`);
        }

        return true;
    }

    _rx_server() {
        // Retransmit the response if we already have one, do nothing if
        // we do not have a response yet. FIXME: send a provisional
        // response to stop retransmissions if we do not have a response
        // yet.
        if (this.response) {
            // Reset the transaction timeout only if we have at least
            // provisional response to send. If we have no provisional
            // response, timeout even if the client keeps retransmitting.
            // It's the responsibility of the higher layer to ensure there
            // is a provisional response for long-lived transactions.

            // Reseting the timer here makes the server transaction live
            // indefinitely as long as the client keep retransmitting the
            // request. FIXME: This is a potential problem if the client is
            // malicious, we may eventually need to implement a transaction
            // timeout that cannot be overriden by the client.
            this._resetTimeout();
            this._send(this.response);
        }
        return true;
    }

    /**
     * The main dispatcher method called by the transaction manager when
     * a request or response should be processed by the transaction (the
     * message has matching transaction ID). Returns true if the message
     * was consumed by this transaction and false otherwise.
     *
     * This function handles two cases: For server-side transaction it
     * re-transmits the response (if one exists). For client side
     * transactions it resolves the transaction when a response has been
     * received.
     */
    rx(m) {
        if (!Array.isArray(m.via)) return false;
        if (m.via.length == 0) return false;

        if (this._client) return this._rx_client(m);
        else return this._rx_server(m);
    }
}
