"""Custom JSON extension functions

To add JSON support to your custom class simply add JSONMixin to the base
classes and override __json__ and __from_json__ methods as needed. If you need
to customize the list of attributes converted then you can create a class
variable __json_attrs__ and initialize it to a list of attribute names. To
omit attributes whose values are None from JSON output/input create the class
variable __json_empty__ and set it to False.

The default implementation of __json__ recursively calls __json__ on
attributes that implement the method and on elements in primitive lists.
"""
from datetime    import datetime
from .utils      import datetime2iso
from .dictutils  import *

__all__ = ['to_dict', 'JSONMixin']


# Convert an object or a list of objects into a primitive representation
# suitable for JSON encoding. The function calls __json__ on objects that
# implement the method and iteratively converts elements in primitive lists.
#
def to_dict(o):
    if isinstance(o, list) or isinstance(o, tuple):
        # If the object is a list, run the same conversion function
        # iteratively on all elements within the list.
        return [to_dict(x) for x in o]
    elif isinstance(o, dict):
        # If the object is a dictionary, recursively convert all attribute
        # values using this function and return the resulting dictionary.
        return dict([(k, to_dict(v)) for k, v in list(o.items())])
    elif isinstance(o, datetime):
        # Convert datetime objects into standard ISO representation
        return datetime2iso(o)
    else:
        if hasattr(o, '_to_dict_'):
            return o._to_dict_()
        else:
            return o


class JSONMixin(object):
    def _to_dict_(self, attrs=None, keep_empty=None, *args, **kw):
        if attrs is None:
            attrs = getattr(self, '_json_attrs_', None)

        res = dict()
        if attrs is not None:
            # If we were given a list of attributes, convert only the selected
            # attributes and ignore the rest.
            for k in attrs:
                try:
                    v = getattr(self, k)
                except AttributeError:
                    pass
                else:
                    res[k] = to_dict(v)
        else:
            # No attribute lists specified, so output all the attributes whose
            # names do not start with _.
            for k in self.__dict__:
                if k[0] != '_':
                    res[k] = to_dict(getattr(self, k))

        # Optionally, remove attributes with empty (None / null) values from
        # the result.
        if not keep_empty:
            if not getattr(self, '_json_empty_', None):
                res = omit_empty(res)

        return res

    def _from_dict_(self, data, attrs=None, keep_empty=None):
        if not keep_empty:
            if not getattr(self, '_json_empty_', None):
                data = omit_empty(data)

        if attrs is None:
            attrs = getattr(self, '_json_attrs_', None)

        if attrs is not None:
            data = pick(data, *attrs)

        return data
