from Crypto import Random


def timed_cmp(in1, in2):
    """Compare two inputs with time complexity independent of string values.

    This function compares two strings in a manner that makes it harder to
    execute a timing attack. The function always scans the entire string,
    regardless of whether a mismatch was found. The function is suitable for
    comparison of cryptographic hashes in signed cookies.
    """
    eq = len(in1) == len(in2)
    if eq:
        res = 0
        left = in1
    else:
        res = 1
        left = in2
    for x, y in zip(left, in2):
        res |= ord(x) ^ ord(y)
    return res == 0


def mine_salt(len=8):
    """Generate a new salt string."""
    return Random.new().read(len)
