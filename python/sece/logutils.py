'''Setup a UNIX-style logger using the standard logging package.

Use this module to setup the logging module to log messages to syslog,
standard output and standard error output. Call function 'setup' to install
the root loggers. The syslog logger by default logs all messages to syslog
with facility daemon. The standard output logger logs all messages below the
threshold ERROR to the standard output and messages with levels ERROR and
CRITICAL to the standard error output.

The module also installs a custom sys.excephook handler to replace the default
one which only prints uncaught top-level exceptions to stderr. The new version
logs top-level uncaught exceptions to both syslog and the standard error
output (if configured). Uncaught top-level exceptions are logged on the
CRITICAL level.
'''
import os
import sys
import traceback
import socket
import logging
import weakref
import threading
from logging.handlers import SysLogHandler

__all__ = ['setup', 'set_stream']


_NAME = ''
_stdout_handler = None
_stdout_filter = None
_stderr_handler = None


class TLStreamHandler(logging.StreamHandler):
    # A thread-local storage based version of logging.StreamHandler. This
    # version makes it possible to override the stream to use for logging on
    # per-thread basis. A weak-reference to stream passed to setStream is
    # stored in thread-local storage and that stream is used for all logging
    # within the thread that called setStream. If setThread wasn't called in a
    # thread, the logger falls back to the stream configured in the
    # constructor.

    def __init__(self, *args, **kwargs):
        super(TLStreamHandler, self).__init__(*args, **kwargs)
        self._tls = threading.local()

    def __getattribute__(self, name):
        # If the caller wants the stream attribute, first consult the
        # thread-local storage. If it doesn't contain a thread-local stream or
        # if the stream object is no longer alive, return the initial stream
        # object set by the constructor when the StreamHandler was created.
        if name == 'stream':
            try:
                obj = self._tls.stream()
                if obj is not None:
                    return obj
            except AttributeError:
                pass
        return super(TLStreamHandler, self).__getattribute__(name)

    def setStream(self, stream):
        # Store a weak reference to the given stream in a thread-local
        # storage. This makes it possible to configure different logging
        # streams for each thread/greenlet.
        self._tls.stream = weakref.ref(stream)


class StdoutFilter(object):
    '''Logging filter which lets records with level below threshold through.

    This filter is used to filter messages for the standard output. Since we
    log records with error and critical levels to the standard error output,
    we should filter them out from the standard output to prevent such
    messages from being logged twice.

    The standard logging module can only filter records whose level is above
    certain threshold, hence the need for an "inverse" custom filter.
    '''
    def __init__(self, threshold):
        self.threshold = threshold

    def filter(self, record):
        if record.levelno >= self.threshold:
            return 0
        else:
            return 1


def log_hook(type_, value, tb):
    '''Log uncaught top-level exceptions with the system logger.

    Instead of printing the stack trace and exception info to the standard
    output as sys.excepthook does it, this function logs exceptions with the
    system logger at the critical level. This ensures that uncaught exceptions
    are logged to syslog in detached mode (which has no stderr).
    '''
    logger = logging.getLogger(_NAME)
    msg = traceback.format_exception(type_, value, tb)
    for line in msg:
        for l in line.rstrip().split('\n'):
            logger.critical(l)


def parse_syslog_addr(addr):
    args = dict()
    parsed = addr.split(':')
    scheme = parsed[0].lower()

    if scheme == 'udp':
        args['socktype'] = socket.SOCK_DGRAM
        try:
            port = int(parsed[2])
        except IndexError:
            port = 514
        args['address'] = (parsed[1], port)
    elif scheme == 'tcp':
        args['socktype'] = socket.SOCK_STREAM
        try:
            port = int(parsed[2])
        except IndexError:
            port = 514
        args['address'] = (parsed[1], port)
    elif scheme == 'unix':
        args['address'] = parsed[1]
    else:
        args['address'] = addr
    return args


def add_syslog_handler(fmt=None, syslog_facility=SysLogHandler.LOG_DAEMON,
                       syslog_level=None, syslog_addr='/dev/log', **config):
    '''Install a root logger to log messages to syslog.
    '''
    root = logging.getLogger('')
    if fmt is None:
        fmt = '%(name)s[%(process)d:%(threadName)s] %(levelname)-5.5s: %(message)s'
    formatter = logging.Formatter(fmt)

    if isinstance(syslog_facility, str):
        syslog_facility = SysLogHandler.facility_names[syslog_facility]

    addr = parse_syslog_addr(syslog_addr)
    handler = SysLogHandler(facility=syslog_facility, **addr)
    handler.setFormatter(formatter)
    if syslog_level is not None:
        handler.setLevel(syslog_level)
    root.addHandler(handler)


def add_stream_handlers(fmt=None, stdout=sys.stdout, stderr=sys.stderr,
                       threshold=logging.ERROR):
    '''Install root loggers to log messages to stdout/stderr or other stream

    If both stdout and stderr stremas are provided, messages with log level
    below 'threshold' are logged to the standard output, mesages with log
    level above (including) the threshold are logged to the standard error
    output. If only one stream is provided, all messages will go to that
    stream. If no streams are provided, sys.stdout and sys.stderr are used.
    '''
    root = logging.getLogger('')
    if fmt is None:
        fmt = '%(asctime)s [%(name)s:%(process)d:%(threadName)s] %(levelname)-5.5s: %(message)s'
    formatter = logging.Formatter(fmt)

    # Messages below threshold (debug, info, and warn by default) go to
    # stdout.
    global _stdout_handler, _stdout_filter
    _stdout_handler = TLStreamHandler(stdout)
    _stdout_handler.setFormatter(formatter)
    _stdout_filter = StdoutFilter(threshold)
    _stdout_handler.addFilter(_stdout_filter)
    root.addHandler(_stdout_handler)

    # Messages above the threshold (error and critical in default
    # configuration).

    # error and critical log levels go to stderr
    global _stderr_handler
    _stderr_handler = TLStreamHandler(stderr)
    _stderr_handler.setFormatter(formatter)
    _stderr_handler.setLevel(threshold)
    root.addHandler(_stderr_handler)

    set_stream(stdout, stderr)


def setup(verbose=0, stream=False, syslog=True, excepthook=True, **config):
    '''Setup the root logger for UNIX-style logging.

    If no_syslog is False the install a root logger to log all messages to
    syslog. If stdout is True then also install a root logger to log all
    messages to the standard output and the standard error output.

    The parameter 'verbose' determines the minimum log level logged
    by both syslog and stdout handlers:
      * 0: WARNING
      * 1: INFO
      * 2: DEBUG

    This function is supposed to receive its keyword parameters in form of an
    unfolded dictionary returned by the argument parser.
    '''
    # Reset all handlers
    root = logging.getLogger('')
    root.handlers = []

    global _NAME
    _NAME = os.path.basename(sys.argv[0])

    if syslog:
        add_syslog_handler(**config)
    if stream:
        add_stream_handlers()

    if not syslog and not stream:
        # Neither syslog nor streams are used for logging, add a dummy root
        # handler to send everything to /dev/null.
        root.addHandler(logging.NullHandler())

    if verbose == 0:
        root.setLevel(logging.WARNING)
    elif verbose == 1:
        root.setLevel(logging.INFO)
    elif verbose == 2:
        root.setLevel(logging.DEBUG)
    else:
        root.setLevel(1)

    # Override the default top-level exception logger (which logs to
    # sys.stderr) with a version that uses the system logger.
    if excepthook:
        sys.excepthook = log_hook


def set_stream(stdout, stderr):
    global _stdout_handler, _stderr_handler

    _stdout_handler.setStream(stdout)
    _stderr_handler.setStream(stderr)
