import dateutil.parser
from   datetime import datetime


def obj_name(obj):
    """Return the name of the object (instance, class, function).

    Returns None of the name of the given object cannot be obtained.
    """
    try:
        return obj.__name__
    except AttributeError:
        try:
            return obj.__class__.__name__
        except AttributeError:
            pass


def fq_name(obj):
    """Return the fully qualified name of the object (instance or class).

    The fully qualified name consists of the name of the package, followed
    by "." and the objects name.
    """
    name = obj_name(obj)
    if name is not None:
        try:
            mod = obj.__module__
        except AttributeError:
            try:
                mod = obj.__class__.__module__
            except AttributeError:
                mod = None
        if mod is None or mod == str.__module__ or mod == "__main__":
            # Ignore module if it is None, builtin, or main.
            return name
        return mod + "." + name


def datetime2iso(val):
    v = val.isoformat()
    if val.tzinfo is None:
        v += 'Z'
    return v


def iso2datetime(val):
    if isinstance(val, datetime):
        return val
    else:
        return dateutil.parser.parse(val)
