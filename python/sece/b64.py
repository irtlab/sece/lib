from base64 import urlsafe_b64encode, urlsafe_b64decode

import logging

log = logging.getLogger(__name__)

def encode(s, padding=False):
    """Encode given byte string into base64 safe for inclusion in URLs.

    If padding is set to True then the resulting string will be padded at
    the end if necessary. If padding is set to False (default) then padding
    will be omitted. This function uses URL-safe base64 alphabet.
    """
    res = urlsafe_b64encode(s)
    if not padding:
        return res.strip(b'=')
    else:
        return res


def decode(s):
    """Decode base64 encoded byte string and return the resulting byte string.

    This function accepts base64-encoded input with the URL-safe alphabet.
    The input does not need to be padded.
    """
    if isinstance(s, str):
        s = s.encode('ascii', 'ignore')
    return urlsafe_b64decode(s + b'=' * (-len(s) % 4))
