"""Dictionary manipulation functions.
"""

def pick(d, *attrs):
    """Remove all attrs from dictionary d which are not in list attrs.

    Returns a copy of the original dictionary.
    """
    return dict((k, d[k]) for k in attrs if k in d)


def omit(d, arg):
    '''Return a copy of the dictionary with blacklisted attributes removed.

    The blacklist provided in the argument arg can take a number of forms:
      - A string: the attribute with matching name will be omitted.
          >>> omit({'a': 1, 'b': 10, 'c': 'aa', 'd': None}, 'a')
          {'b': 10, 'd': None, 'c': 'aa'}
      - A list or tuple: attributes with names matching the strings in the
        list or tuple will be omitted.
          >>> omit({'a': 1, 'b': 10, 'c': 'aa', 'd': None}, ['a', 'b'])
          {'d': None, 'c': 'aa'}
      - A predicate function: attributes for which the function returns true
        will be omitted. The function receives the source dictionary and the
        key as positional arguments.
          >>> omit({'a': 1, 'b': 10}, lambda d,k: d[k] < 10)
          {'b': 10}
    '''
    if type(arg) is list or type(arg) is tuple:
        return dict((k, d[k]) for k in d if k not in arg)
    elif isinstance(arg, str):
        return dict((k, d[k]) for k in d if k != arg)
    else:
        return dict((k, d[k]) for k in d if arg(d, k) is not True)


def omit_empty(d):
    """Omit attributes whose values are None.

    Returns a copy of the original dictionary.
      >>> omit_empty({'a': 1, 'b': 10, 'c': 'aa', 'd': None})
      {'b': 10, 'a': 1, 'c': 'aa'}
    """
    return omit(d, lambda d, k: d[k] is None)
