import logging
import hashlib
import simplejson as json
from   functools  import wraps

from redis import *

from .b64   import encode as base64_encode
from .utils import fq_name

__all__ = ['connect_redis', 'redis_cache']

log = logging.getLogger(__name__)


def connect_redis(uri='unix://@/run/redis/redis.sock'):
    c = StrictRedis.from_url(uri)
    # Make sure we're really connected, raises exception if not. This is to
    # catch common configuration mistakes, such as too restrictive permissions
    # on a UNIX domain socket.
    c.ping()
    return c


def _get_value(param, f, args, keywords, result=None):
    """Obtain the value for parameter param.

    The mechanims for obtaining the value depends on the type of the value
    param is set to:
      * If param is a callable then its return value will be used.
      * If param is {'name'} then the value is obtained from the keyword
        argument with name 'name'
      * If param is [number] then the value is obtained from the positional
        argument at index number
      * Other types of values are returned verbatim.
    """
    if param is None:
        return
    if hasattr(param, '__call__'):
        return param(f, args, keywords, result)
    elif isinstance(param, set):
        if len(param) != 1:
            raise ValueError('Set must contain exactly one item: %s',
                repr(param))
        for v in param:
            return keywords[v]
    elif isinstance(param, list):
        if len(param) != 1:
            raise ValueError('List must contain exactly one item: %s',
                repr(param))
        return args[param[0]]
    else:
        return param


def _build_redis_key(prefix, param, f, args, keywords):
    """Generate a REDIS key from given parameters.

    If a value is provided for param then we use that value, optionally
    prefixing it with the value of prefix.

    If no value is provided for param then the function generates a
    signature for the given function f along with its arguments
    (args, keywords) and uses the signature as the key. The signature
    includes:
      - Fully qualified name of the function
      - Unique ID of the function (unique per closure, captures free
        parameters)
      - a Hash of all (positional and keyword) parameters.
    """
    if prefix is None:
        prefix = ''
    if param is not None:
        return '%s%s' % (prefix, param)
    else:
        m = hashlib.md5(repr(args))
        m.update(repr(keywords))
        sig = base64_encode(m.digest())
        return '%s%s/%s/%s' % (prefix, fq_name(f), id(f), str(sig))


def redis_cache(prefix=None, key=None, expires=None):
    """Decorator @redis_cache().

    When a function is decorated with this decorator, the implementation
    first checks the REDIS cache to see if it contains the return value
    for the wrapped function with the given parameters. If a value was
    found then it is returned without calling the wrapped function. If no
    value was found in REDIS then the function is called and its return
    value is stored in REDIS.

    When used without arguments the decorator
    """
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kw):
            k = _get_value(key, f, args, kw)
            k = _build_redis_key(prefix, k, f, args, kw)
            rv = redis.get(k)
            if rv is None:
                rv = f(*args, **kw)
                e = _get_value(expires, f, args, kw, rv)
                if e is not None:
                    redis.setex(k, e, rv)
                else:
                    redis.set(k, rv)
            return rv
        return wrapper
    return decorator
