import logging
import simplejson as json

from zope.dottedname.resolve import resolve

from .errors import SECError

log = logging.getLogger(__name__)


class MessageError(SECError):
    """The root of all errors related to API message processing.
    """

class ParseError(MessageError):
    """Raised when the fixed header of a message cannot be parsed.

    When this error is raised the implementation should reset the connection
    the message was received on. This exception is raised during the beginning
    of message processing when we're not even sure that we received anything
    that resembles a valid message.
    """

class RequestError(MessageError):
    """Raised when on error while a request was being processed.

    This exception is raised when we have a valid message header and the
    message is a request. Message errors can be caused by errors on client
    side, transport errors, or server-side errors.
    """
    def __init__(self, request, code, reason, body=None):
        super(RequestError, self).__init__()
        self.request = request
        self.code = code
        self.reason = reason
        self.body = body

    def __str__(self):
        if self.request.request:
            response = [self.request.channel, 'error', self.code, self.reason]
            if self.body:
                response.append(self.body)
            return json.dumps(response)
        else:
            # Do not generate an error response on an error response, send
            # the original response instead
            return self.request._input


class ClientError(RequestError):
    """Raised when the client provided invalid request data.

    This error is raised when the request received from a client is malformed
    or cannot be processed due to some problem caused by the request received
    from the client.
    """

class TransportError(RequestError):
    """Raised on errors while a request is being transported.

    Instances of this exception are raised when the source of the error is
    between the client and the server, for example, when the request cannot be
    routed towards its destination.
    """

class ServerError(RequestError):
    """Raised when the function invoked on the server raises an exception.

    The original exception raised by the called function is converted to
    string and the result is set as the reason text of ServerError.
    """
    def __init__(self, request, orig_exc):
        super(ServerError, self).__init__(request, 500, str(orig_exc))


class Message(object):
    """Messages exchanged over WebSocket or API connections.

    Each message is an array where the first element of the array
    """
    def __init__(self, input=None):
        if input:
            self.parse(input)

    def parse(self, input):
        # FIXME: Use an incremental JSON parser to parse the fixed
        # beginning of the message so that we can at least generate
        # proper error responses (for which we need the message type
        # and tag)
        try:
            self._parsed = json.loads(input)
        except ValueError:
            raise ParseError()
        if not isinstance(self._parsed, (list, tuple)):
            raise ParseError()
        try:
            self.channel = self._parsed[0]
            self.type = self._parsed[1]
        except IndexError:
            raise ParseError()

        self._input = input

        if not isinstance(self.channel, str):
            raise ParseError()

        if not isinstance(self.type, str):
            raise ParseError()

        self.type = self.type.lower()
        if self.type == 'error' or self.type == 'response':
            self.request = False
        else:
            self.request = True

        if self.request:
            try:
                self.name = self._parsed[2]
            except IndexError:
                raise ClientError(self, 400, 'Name missing')
            if not isinstance(self.name, str):
                raise ClientError(self, 400, 'Unsupported name format')

    def __str__(self):
        try:
            return json.dumps(self._parsed)
        except TypeError:
            raise TransportError(self, 500, 'Request not serializable')


class Response(object):
    def __init__(self, msg, body):
        if not msg.request:
            raise SECError("Can't generate response for a response")
        self.request = msg
        self.body = body

    def __str__(self):
        try:
            return json.dumps([self.request.channel, 'response', self.body])
        except TypeError:
            return str(TransportError(self.request, 500, 'Result not serializable'))


class APIHandler(object):
    def __init__(self, prefix=None, module=None):
        self.prefix = prefix
        self.module = module

    def invoke(self, request):
        try:
            name = request.name
            if self.prefix:
                if not name.startswith(self.prefix):
                    raise NameError('Unsupported command %s' % name)
                name = name[len(self.prefix):]
            log.debug('name: %s' % name)
            if self.module:
                name = '%s%s' % (self.module, name)
            log.debug('name: %s' % name)

            log.debug('Resolving "%s"' % name)
            func = resolve(name)
        except ImportError:
            raise ClientError(request, 404, 'Function not found')

        try:
            args, kwargs = request._parsed[3]
        except IndexError:
            args = []
            kwargs = {}
        except ValueError:
            raise ClientError(request, 400, 'Invalid function arguments')

        try:
            return Response(request, func(*args, **kwargs))
        except Exception as e:
            return ServerError(request, e)

    def process_message(self, data):
        msg = Message(data)
        if msg.request:
            return self.process_request(msg)
        else:
            raise MessageError('Unexpected response message received')

    def process_request(self, request):
        if request.type == 'invoke':
            return self.invoke(request)
        else:
            raise NotImplemented('Request type %s not implemented' % request.type)
