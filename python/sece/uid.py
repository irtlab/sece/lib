import uuid

from sqlalchemy.types    import TypeDecorator
from sqlalchemy.dialects import postgresql

from .json import JSONMixin
from .b64  import (
    decode as base64_decode,
    encode as base64_encode)

#
# Add support for instantiation with the first argument being another UID
# object.
#
class UID(uuid.UUID, JSONMixin):
    """Randomly generated (UUID type 4) unique identifier.

    This is an extension of the UUID class from Python's uuid module.

    To create a new randomly generated unique identifier create a new
    instance of the class without any constructor arguments:
      >>> UID()
      UID('b8719038-47ed-43c7-8509-61b8900aa64d')

    To obtain a base64 encoded value of a UID use:
      >>> UID().base64
      'T1n5XYYdSTWDWCLxo6bQpg'

    The class also supports creating a UID object from a specific
    base64 encoded (both with and without padding) input value:

      >>> UID('T1n5XYYdSTWDWCLxo6bQpg')
      UID('4f59f95d-861d-4935-8358-22f1a3a6d0a6')
    or
      >>> UID(base64='T1n5XYYdSTWDWCLxo6bQpg')
      UID('4f59f95d-861d-4935-8358-22f1a3a6d0a6')

    For description of other parameters and methods see the documentation
    on Python uuid module.
    """

    def __new__(cls, *args, **kw):
        '''A UID constructor which returns None if given None as argument.

        This function passes whatever UID representation it is given on
        input and returns an UID instance or None (if given None). On
        parsing error the function raises an exception.

        Call this function on UID values received over the network or
        from the user to ensure the data is properly formatted.
        '''
        if len(args) == 1 and args[0] is None and not kw:
            return None
        else:
            return super(UID, cls).__new__(cls)

    def __init__(self, *args, **kw):
        return self.parse(*args, **kw)

    def parse(self, *args, **kw):
        if not args and not kw:
            # No arguments given, create a new random unique id
            return uuid.UUID.__init__(self, bytes = uuid.uuid4().bytes)

        val = None
        if len(args) == 1 and isinstance(args[0], str):
            n = len(args[0])
            if n == 22 or n == 24:
                # 22 (withou padding) and 24 (with padding) are sizes of
                # a base64 encoded 128-bit value.
                val = base64_decode(args[0])

        if val is None and (kw and 'base64' in kw):
            val = base64_decode(kw['base64'])

        if val:
            if len(val) != 16:
                raise ValueError("Base64 encoded value must be 16 bytes long.")
            else:
                return uuid.UUID.__init__(self, bytes = val)

        return uuid.UUID.__init__(self, *args, **kw)

    @property
    def base64(self):
        """Return base64 encoded (without padding) Unique ID"""
        return base64_encode(self.bytes)

    @property
    def uuid(self):
        return uuid.UUID.__str__(self)

    def __repr__(self): return "UID('%s')" % self.uuid
    def __str__(self):  return self.base64.decode('utf-8')

    def _to_dict_  (self, request=None): return self.__str__()
    def _from_dict_(self, input):        return self.parse(base64=input)



class UIDColumn(TypeDecorator):
    '''SQLAlchemy type decorator for UID.

    UID objects inherit from Python uuid.UUID, so we simply encode them into
    hexa-formated UUID strings when storing them in the database and then back
    into the binary format when reading back. PostgreSQL internally stores
    UUIDs as 128-bit binary integers, but accepts and outputs them in their
    textual representation only.
    '''
    impl = postgresql.UUID

    def process_bind_param(self, val, dialect):
        if val is None:
            return val
        if isinstance(val, str):
            # String values may come in a variety of supported formats so
            # we need to parse it and convert the value into the canonical
            # hexademical form.
            return UID(val).uuid
        if isinstance(val, UID):
            return val.uuid
        raise ValueError('Unsupported value type: ' % type(val))

    def process_result_value(self, val, dialect):
        if val is not None:
            val = UID(val)
        return val
