#!/usr/bin/env python3
import os
from setuptools import setup, find_packages
from subprocess import check_output as run, CalledProcessError


if os.path.isdir('../.git'):
    try:
        version = run('git describe --always --tags --dirty', shell=True)
        f = open('VERSION', 'wb')
        f.write(version.strip())
        f.close()
    except (CalledProcessError, OSError):
        pass


setup(name='sece-lib',
    version = open('VERSION').read().strip(),
    description = 'SECE Python support library',
    author = 'Jan Janak',
    author_email = 'jan@janakj.org',
    install_requires = [
        'redis',
        'simplejson',
        'sqlalchemy',
        'pycrypto',
        'zope.dottedname'
    ],
    namespace_packages = ['sece'],
    packages = find_packages(),
)
