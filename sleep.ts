export default function sleep(ms: number, ...args: any[]) {
    return new Promise(resolve => setTimeout(resolve, ms, ...args));
}
