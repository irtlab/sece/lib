import URI    from 'urijs';
import {GUID} from './guid';


export function NodeURI(scheme, node) {
    this.scheme = scheme;
    this.node = node;
}

NodeURI.prototype.toString = function() {
    return `${this.scheme}:${this.node}`;
};

NodeURI.prototype.toJSON = NodeURI.prototype.toString;


NodeURI.parse = function(uri) {
    let s = uri.toString();

    if (s.startsWith('sece:')) s = s.substr(5);
    return new NodeURI('sece', s);
};

NodeURI.random = function() {
    return new NodeURI('sece', GUID());
};



export function ResourceURI(scheme, node, resource) {
    this.scheme = scheme;
    this.node = node;
    if (resource) this.resource = resource;
}

ResourceURI.prototype.toString = function() {
    return `${this.scheme}://${this.node}/${this.resource ? this.resource.toString() : ''}`;
};

ResourceURI.prototype.toJSON = ResourceURI.prototype.toString;


ResourceURI.parse = function(uri) {
    let u = new URI(uri);
    if (u.scheme() !== 'sece') {
        let e = new Error('Unsupported Resource URI scheme');
        e.uri = uri;
        throw e;
    }

    if (!u.hostname()) {
        let e = new Error('Invalid or missing node ID in Resource URI');
        e.uri = uri;
        throw e;
    }

    let res = u.resource();
    if (res[0] === '/') res = res.substr(1);

    return new ResourceURI(u.scheme(), u.hostname(), res ? res : undefined);
};
