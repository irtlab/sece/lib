/**
 * @module @sece/lib/ws
 *
 * This module provides several WebSocket wrappers with HTML5
 * compatible interface.
 *
 * SRVWebSocket resolves the given URI using DNS SRV and attempts to
 * connect to the resulting records sequentiallly according to the
 * algorithm described in the DNS SRV RFC.
 *
 * ReconnectingWebSocket implements a websocket that keeps
 * reconnecting automatically when disconnected. Implements
 * exponential reconnecting backoff with randomization. The socket
 * will keep reconnecting forever until the user calls the close
 * method.
 *
 * Reconnection timeouts are currently not configurable. Unlike
 * similar existing node libraries, this implementation is really
 * simple.
 *
 * TODO
 *   - Shorter (configurable) timeout for unreachable hosts in SRVWebSocket
 *
 */

import {EventEmitter2}           from 'eventemitter2';
import URI                       from 'urijs';
import {resolveSRV, SRVIterator} from './dns';

let debug, getPID;

const minReconnectDelay  = 1000,
    maxReconnectDelay    = 10000,
    reconnectDelayFactor = 1.3;

const SRV_PREFIX = '_wss._tcp.';


/**
 * The URIjs library won't be able to parse a URI without //
 * characters. Insert those characters into the URI so that the
 * library can parse it into components. This is a hackish solution.
 */
function mangleURI(uri) {
    const sep = uri.indexOf(':');
    if (sep == -1)
        throw new Error(`Invalid URI ${uri}`);

    const scheme = uri.slice(0, sep),
        // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
        body = uri.slice(sep + 1);

    return `${scheme}://${body}`;
}


/**
 * The WebSocketInterface class is a wrapper over the standard HTML5
 * WebSocket. The wrapper adds a couple of convenient features:
 * - It can provide stable object reference across changing HTML5
 *   WebSocket objects
 * - It provides an EventEmitter2 interface which makes it possible
 *   for multiple listeners to get notified on open, close, error, and
 *   message events.
 *
 * The above functionality is particularly useful for websockets that
 * automatically reconnect. The HTML5 API would force the application
 * to create a new WebSocket object each time the socket is connected.
 * Unfortunately, that makes it difficult to work with such objects
 * because there is no persistent object to, e.g., keep a list of
 * socket listeners.
 *
 * Invoke the method _delegate to set the underlying HTML5 WebSocket
 * object. The object will be stored in the internal _sock property.
 *
 * This class provides all the well-known getters and methods of HTML5
 * WebSocket which makes the wrapper usable as a drop-in replacement
 * for HTML5 WebSocket objects.
 */
class WebSocketInterface extends EventEmitter2 {
    constructor() {
        super({ newListener: true });

        this._onopen = this._onopen.bind(this);
        this._onclose = this._onclose.bind(this);
        this._onerror = this._onerror.bind(this);
        this._onmessage = this._onmessage.bind(this);
        this._onNewListener = this._onNewListener.bind(this);

        // TODO: We should also catch event listeners added via onAny
        // here, but the library does not seem to be triggering
        // newListener in that case.
        this.on('newListener', this._onNewListener);
    }

    _onNewListener(event, func) {
        if (event === 'open') {
            if (typeof this._sock === 'undefined') return;
            if (this.readyState == this.OPEN) func();
        }

        if (event === 'close') {
            if (typeof this._sock === 'undefined' || this._sock.readyState == this.CLOSED)
                func();
        }
    }

    _onopen(...args) {
        if (typeof this.onopen === 'function') this.onopen(...args);
        this.emit('open', ...args);
    }

    _onclose(...args) {
        if (typeof this.onclose === 'function') this.onclose(...args);
        this.emit('close', ...args);
    }

    _onerror(...args) {
        if (typeof this.onerror === 'function') this.onerror(...args);
        this.emit('error', ...args);
    }

    _onmessage(...args) {
        if (typeof this.onmessage === 'function') this.onmessage(...args);
        this.emit('message', ...args);
    }

    _delegate(sock) {
        this._undelegate();

        sock.onclose   = this._onclose;
        sock.onerror   = this._onerror;
        sock.onmessage = this._onmessage;
        sock.onopen    = this._onopen;

        this._sock = sock;

        /* If we were given an open websocket, invoke the onopen handler
        * to make sure that any existing subscribers do not miss that
        * event.
        */
        if (sock.readyState == sock.OPEN) this._onopen();
    }

    _undelegate() {
        if (typeof this._sock === 'undefined') return;

        /* If we were not in CLOSED state and the websocket is being
        * removed, invoke the onclose handler to let existing subscribers
        * know that we're in closed state now.
        */
        // eslint-disable-next-line no-undef
        if (this.readyState !== this.CLOSED) this._onclose(new CloseEvent('close', {code: 1006}));

        this._sock.onclose = null;
        this._sock.onerror = null;
        this._sock.onmessage = null;
        this._sock.onopen = null;
        delete this._sock;
    }

    get binaryType()     { return this._sock.binaryType; }
    get bufferedAmount() { return this._sock ? this._sock.bufferedAmount : 0; }
    get extensions()     { return this._sock.extensions; }
    get protocol()       { return this._sock.protocol; }
    get readyState()     { return this._sock ? this._sock.readyState : this.CLOSED; }
    get url()            { return this._sock ? this._sock.url : undefined; }

    send(...args)        { return this._sock.send(...args); }

    close(...args) {
        this.off('newListener', this._onNewListener);
        if (this._sock)
            return this._sock.close(...args);
    }

    /* The getters for the values returned by readyState() */
    // eslint-disable-next-line no-undef
    get CONNECTING() { return WebSocket.CONNECTING; }
    // eslint-disable-next-line no-undef
    get OPEN()       { return WebSocket.OPEN;       }
    // eslint-disable-next-line no-undef
    get CLOSING()    { return WebSocket.CLOSING;    }
    // eslint-disable-next-line no-undef
    get CLOSED()     { return WebSocket.CLOSED;     }
}


/**
 * An SRV WebSocket resolves the given URI using DNS SRV. It attempts
 * to connect to one server after another according to the algorithm
 * defined in the DNS SRV RFC.
 */
export class SRVWebSocket extends WebSocketInterface {
    constructor(uri, ...args) {
        super();
        const parsedURI = new URI(mangleURI(uri));

        if (getPID) {
            parsedURI.query(function(data) {
                data.pid = getPID();
            });
        }

        this._reconnect = () => {
            this._open(parsedURI, ...args).then(({ url, ws }) => {
                debug(`Connected to ${url}`);
                this._delegate(ws);
            }).catch(error => {
                const domain = parsedURI.domain();
                if (error.noService) {
                    debug(`Domain ${domain} does not support WebSockets`);
                } else {
                    debug(`Connection to WebSocket service in domain ${domain} failed`);
                    console.error(error, error.stack);
                }
            });
        };

        this._reconnect();
    }

    /**
     * Given an iterable of websocket URLs, connect to the first server
     * that accepts the connection. Returns the URL and an open socket
     * on success.
     */
    async _connect(uri, srvRecords, ...args) {
        for (const rec of srvRecords) {
            const t = (rec.data.target.slice(-1) === "." ? rec.data.target.slice(0, -1) : rec.data.target),
                u = this._url(uri, t, rec.data.port).toString();

            debug(`Connecting to ${u}`);

            try {
                return await new Promise((resolve, reject) => {
                    // eslint-disable-next-line no-undef
                    const s = new WebSocket(u, ...args);
                    s.onopen  = (...a) => {
                        s.onopen = null;
                        s.onerror = null;
                        s.onclose = null;
                        resolve({ url: u, ws: s, args: a });
                    };
                    s.onerror = () => { reject(); };
                    s.onclose = () => { reject(); };
                });
            } catch(error) { /* continue */ }
        }

        throw new Error();
    }

    async _open(uri, ...args) {
        const domain = uri.hostname();
        try {
            const recs = await resolveSRV(`${SRV_PREFIX}${domain}`);
            // An empty array indicates that the SRV record does not exist or
            // that the target domain explicitly disabled the service.
            if (!recs.length) {
                let e = new Error('No service');
                e.noService = true;
                throw e;
            }

            // Sort the returned records according to the DNS SRV RFC
            // algorithm and return the resulting array.
            const srvRecords = new SRVIterator(recs)[Symbol.iterator]();

            const { url, ws } = await this._connect(uri, srvRecords, ...args);

            // Once we are connected, delegate all callbacks from the
            // connected websocket to the callbacks provided by the higher
            // layer.
            return { url: url, ws: ws };
        } catch(error) {
            // If resolution failed, SRV record was not found, or the domain
            // disabled the service, the callback won't be automatically
            // invoked. Emulate the behavior of the browser on NXDOMAIN,
            // timeout, or connection reset.

            try {
                // eslint-disable-next-line no-undef
                this._onerror(new Event('error'));
            } catch(e) {
                console.error(`onerror: ${e}`, e.stack);
            }

            try {
                // eslint-disable-next-line no-undef
                this._onclose(new CloseEvent('close', {code: 1006}));
            } catch(e) {
                console.error(`onerror: ${e}`, e.stack);
            }

            throw error;
        }
    }

    _url(uri, hostname, port) {
        return new URI({
            protocol : uri.protocol(),
            username : uri.username(),
            password : uri.password(),
            hostname : hostname,
            port     : port,
            query    : uri.query(),
            fragment : uri.fragment()
        });
    }
}


function initReconnectDelay() {
    return minReconnectDelay + Math.random() * minReconnectDelay;
}


function updateReconnectDelay(delay) {
    let n = delay * reconnectDelayFactor;
    return (n > maxReconnectDelay) ? maxReconnectDelay : n;
}


export class ReconnectingSRVWebSocket extends SRVWebSocket {
    constructor(uri, ...args) {
        super(uri, ...args);
        this._onOpen = this._onOpen.bind(this);
        this._onClose = this._onClose.bind(this);

        this.on('open', this._onOpen);
        this.on('close', this._onClose);
    }

    _onOpen() {
        this._delay = initReconnectDelay();
        this._cancelTimer();
    }

    _onClose() {
        if (!this._delay) this._delay = initReconnectDelay();
        else this._delay = updateReconnectDelay(this._delay);

        this._timer = setTimeout(() => {
            this._reconnect();
            this._cancelTimer();
        }, this._delay);
    }

    _cancelTimer() {
        if (this._timer) {
            clearTimeout(this._timer);
            delete this._timer;
        }
    }

    close(...args) {
        this._cancelTimer();
        this.off('close', this._onClose);
        this.off('open', this._onOpen);
        return super.close(...args);
    }
}

export function init(logger, gp) {
    debug = logger;
    debug(`Initializing DNS SRV capable WebSocket client`);
    getPID = gp;
}