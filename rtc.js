import e2                    from 'eventemitter2';
import debug                 from 'debug';
import {NodeURI,ResourceURI} from './uri';
import {DEFAULT}             from './common';
import {Response}            from './message';


// FIXME: This is the old way, but apparently both Chrome and Firefox
// support this format. Switch to the new format eventually.
const media_constraints = {
    optional: [],
    mandatory: {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    }
};

const answer_constraints = media_constraints,
    offer_constraints = media_constraints;


function wait_for_ice(c) {
    let done;
    c.onicecandidate = e => {
        if (!e.candidate) {
            delete c.onicecandidate;
            done();
        }
    };

    return new Promise(resolve => done = resolve);
}


/**
 * Return a string that describes the given stream and that is
 * suitable for inclusion in a signaling message.
 */
function media2hdr(s) {
    let id;
    if (s == true) id = undefined;
    else id = s.id.toString();

    return JSON.stringify({
        type: 'MediaStream',
        id  : id
    });
}


/**
 * Given a string describing a media stream, extract and return the
 * stream's ID.
 */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
function hdr2media(s) {
    const v = JSON.parse(s);
    if (v.type != 'MediaStream') {
        throw new Error('Invalid media stream header');
    }
    return v.id;
}


/**
 * Encode DataChannel parameters into a header value. By default, the
 * value is a JSON encoded object with the most important data channel
 * parameters.
 */
function dc2hdr(c) {
    if (typeof c.id !== 'number')
        throw new Error('Invalid or missing DataChannel ID');

    return JSON.stringify({
        type    : 'DataChannel',
        id      : c.id,
        label   : c.label,
        protocol: c.protocol,
        ordered : c.ordered,
        reliable: c.reliable
    });
}


function hdr2dc(s) {
    const v = JSON.parse(s);
    if (v.type != 'DataChannel') {
        throw new Error('Invalid datachannel header');
    }
    return {
        id: parseInt(v.id),
        label: v.label,
        protocol: v.protocol,
        ordered: (typeof v.ordered == 'string') ? (v.ordered == 'true') : v.ordered == true,
        reliable: (typeof v.reliable == 'string') ? (v.reliable == 'true') : v.reliable == true
    };
}


/**
 * Create an SDP document containing the initial offer to be sent to
 * the remote peer when establishing a new peer connection. The
 * document will contain an SDP offer with an initial set of ICE
 * candidates.
 */
async function create_offer(c) {
    await Promise.all([
        wait_for_ice(c),
        c.createOffer(offer_constraints).then(o => c.setLocalDescription(o))
    ]);

    return c.localDescription.sdp.toString();
}

// FIXME: There is a potential interval here where ICE candidates may
// be lost if they are generated after the initial gathering, but
// before activate_rtcpeerconnection sets up new callbacks.

/**
 * Given an initial offer received from peer, create an initial
 * answer. Returns a promise that resolves to the answer on success.
 *
 * FIXME: The API must trigger the onicecandidate callback one or more
 * times, otherwise this function blocks indefinitely.
 */
async function create_answer(c, offer) {
    await Promise.all([
        wait_for_ice(c),
        c.setRemoteDescription({type: 'offer', sdp: offer})
            .then(() => c.createAnswer(answer_constraints))
            .then(answer => c.setLocalDescription(answer))
    ]);

    return c.localDescription.sdp.toString();
}



// The RTCConnection manager is used to keep track of RTC peer
// connections to various peers. The manager ensures that there is
// only one active connection to each peer. Tehnically, the peer
// stores promises, not connection objects itself. Hence, one needs to
// await the value returned by get. This is because we need to
// register the connection in the manager even before it is
// established to ensure that other attempts to establish a peer
// connection to the same peer do not establish a new connection.

class Manager extends e2.EventEmitter2 {
    constructor() {
        super();
        this.connections = {};
    }

    async set(peer, connection) {
        let prev = this.connections[peer];

        const cleanup = () => {
            if (this.connections[peer] === connection) {
                delete this.connections[peer];
                this.emit('delete', this.connections[peer]);
            }
        };

        if (prev) {
        // FIXME: Temporary disable this to prevent crashes in Node.
        // prev.then(c => c.close());
        }

        this.connections[peer] = connection;
        this.emit('set', connection);

        try {
            await connection.established;
            connection.once('closed', cleanup);
        } catch(e) {
            cleanup();
            throw e;
        }
    }

    get(peer) {
        return this.connections[peer];
    }
}


// The RTCConnection object associates a signaling dialog with an
// RTCPeerConnection object. The object will become established
// (promise resolved) once the dialog has been established and the
// corresponding RTCPeerObject has been initialized. FIXME: We may
// even resolve the promise only after the RTCPeerConnection object
// reports that the transport has been established.

export class RTCConnection extends e2.EventEmitter2 {
    constructor(peer, initiator, rtc_config, opts) {
        super();
        this.peer = peer;
        this.dc_id = initiator ? 2 : 1;
        this.dbg = peer.dbg;

        if (opts) this.opts = {...opts};
        else this.opts = {};

        // eslint-disable-next-line no-undef
        this.connection = new RTCPeerConnection(rtc_config || {});

        this.established = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });
    }

    async close() {
        this._reject('Connection closed');
        this.close_datachannel();
        this.close_rtcpeerconnection();
        await this.close_dialog();
        this.emit('closed');
    }

    close_datachannel() {
        if (!this.channel) return;
        this.channel.onopen = null;
        this.channel.onmessage = null;
        this.channel.onclose = null;
        this.channel.close();
        delete this.channel;
    }

    close_rtcpeerconnection() {
        if (!this.connection) return;

        this.dbg(`Destroying RTCPeerConnection`);
        let c = this.connection;
        c.onconnectionstatechange = null;
        c.onsignalingstatechange = null;
        c.oniceconnectionstatechange = null;
        c.onicegatheringstatechange = null;

        c.onicecandidate = null;
        c.onnegotiationneeded = null;
        c.onaddstream = null;
        c.ondatachannel = null;

        if (c.signalingState != "closed") c.close();
        delete this.connection;
    }

    async close_dialog() {
        if (!this.dialog) return;
        try {
            await this.dialog.bye();
        } catch(e) {
            console.log(e, e.stack);
        }
        delete this.dialog;
    }

    async open(dialog) {
        let tag = dialog.id.slice(0, 6);
        this.dbg = debug(`sece:R[${tag}]`);

        this.setup_rtcpeerconnection_callbacks();

        this.dialog = dialog;
        this.setup_dialog_callbacks(dialog);
        this.activate_rtcpeerconnection();

        try {
            await this.dialog.established;

            this.dialog.once('closed', () => {
                this.dbg('Dialog closed, closing RTCConnection');
                void this.close();
            });

        } catch(e) {
            this._reject(e);
        }
    }

    setup_rtcpeerconnection_callbacks() {
        this.connection.onaddstream = e => {
            this.dbg(`onaddstream: ${e.stream.id}`);
            this.emit('onaddstream', e);
        };

        this.connection.ondatachannel = e => {
            this.dbg(`ondatachannel: ${e.channel.id}`);
            this.emit('ondatachannel', e);
        };

        this.connection.onconnectionstatechange = () => {
            this.dbg(`RTCPeerConnection state: ${this.connection.connectionState}`);
            this.emit('onconnectionstatechange', this.connection.connectionState);
        };
        this.connection.onconnectionstatechange();

        this.connection.onsignalingstatechange = () => {
            this.dbg(`Signaling state: ${this.connection.signalingState}`);
            this.emit('onsignalingstatechange', this.connection.signalingState);
        };
        this.connection.onsignalingstatechange();

        this.connection.oniceconnectionstatechange = () => {
            this.dbg(`ICE connection state: ${this.connection.iceConnectionState}`);
            this.emit('oniceconnectionstatechange', this.connection.iceConnectionState);
            if (this.connection.iceConnectionState === 'connected')
                this._resolve(this);
            if (this.connection.iceConnectionState === 'failed') {
                this._reject(this);

                if (this.opts.closeDialogOnICEFail && this.dialog) {
                    this.dbg('ICE failed, disconnecting dialog');
                    this.dialog.bye();
                }
            }
        };
        this.connection.oniceconnectionstatechange();

        this.connection.onicegatheringstatechange = () => {
            this.dbg(`ICE gathering state: ${this.connection.iceGatheringState}`);
            this.emit('onicegatheringstatechange', this.connection.iceGatheringState);
        };
        this.connection.onicegatheringstatechange();
    }

    activate_rtcpeerconnection() {
        this.connection.onicecandidate = async (data) => {
            if (!data.candidate) {
                this.dbg(`No more ICE candidates`);
                return;
            }
            this.dbg(`Got ICE candidate: ${JSON.stringify(data.candidate)}`);

            try {
                await this.send_ice(data.candidate);
            } catch(e) {
                console.error(e, e.stack);
            }
        };

        this.connection.onnegotiationneeded = async () => {
            this.dbg('Negotiation needed');

            try {
                await this.offer_answer();
            } catch(e) {
                console.error(e, e.stack);
            }
        };
    }

    async offer_answer() {
        let c = this.connection,
            d = this.dialog;

        let offer = await c.createOffer(offer_constraints);
        await c.setLocalDescription(offer);
        d.dbg('Sending SDP offer');

        let r = d.request(),
            t = d.T(r);
        d.add(t);
        r.method = 'SDP';
        r.body = c.localDescription.sdp.toString();
        t.start();

        let res = await t.resolved;
        if (!res.isSuccess()) {
            const e = new Error(`SDP offer rejected by peer`);
            e.dialog = d;
            e.response = res;
            throw e;
        }

        d.dbg(`Got SDP answer from peer`);
        // eslint-disable-next-line no-undef
        const a = new RTCSessionDescription({type: 'answer', sdp: res.body});
        await c.setRemoteDescription(a);
    }

    async send_ice(candidate) {
        let d = this.dialog;

        let r = d.request(),
            t = d.T(r);
        d.add(t);
        r.method = 'ICE';
        r.body = JSON.stringify(candidate);
        t.start();

        let res = await t.resolved;
        if (!res.isSuccess()) {
            const e = new Error('ICE candidate rejected by peer');
            e.dialog = d;
            e.response = res;
            throw e;
        }
    }

    // FIXME: Setup a timer when delegating the transaction to a media
    // or datachannel server to ensure that we resolve the transaction
    // when something goes wrong in the handlers.
    SYN(req) {
        this.dbg(`Got SYN request`);

        const t = this.dialog.T(req);
        try {
            var sel = JSON.parse(req.body);
        } catch(e) {
            t.send(400, 'Malformed SYN Request Body');
            return;
        }

        switch(sel.type) {
            case 'media': this.peer.SYN_media(this, t, req); return;
            case 'data':  this.peer.SYN_data(this, t, req);  return;
        }

        t.send(501, 'Not Implemented');
    }

    async ICE(req) {
        this.dbg(`Got ICE candidate`);

        try {
            // eslint-disable-next-line no-undef
            var c = new RTCIceCandidate(JSON.parse(req.body));
        } catch(e) {
            console.error(e, e.stack);
            this.dialog.T(req).send(400, 'Error while processing ICE candidate');
            return;
        }

        try {
            await this.connection.addIceCandidate(c);
            this.dialog.T(req).send(200, 'OK');
        } catch(e) {
            console.error(e, e.stack);
            this.dialog.T(req).send(500, 'Error while adding ICE candidate');
        }
    }

    async SDP(req) {
        this.dbg(`Got SDP offer from peer`);

        try {
            // eslint-disable-next-line no-undef
            var offer = new RTCSessionDescription({type: 'offer', sdp: req.body});
        } catch(e) {
            console.error(e, e.stack);
            this.dialog.T(req).send(400, 'Error while processing offer');
            return;
        }

        try {
            await this.connection.setRemoteDescription(offer);

            this.dbg('Generating answer');
            let answer = await this.connection.createAnswer(answer_constraints);
            await this.connection.setLocalDescription(answer);

            let t = this.dialog.T(req),
                res = Response.fromRequest(req, 200, 'OK');

            res.body = this.connection.localDescription.sdp.toString();
            t.send(res);
        } catch(e) {
            console.log(e, e.stack);
            this.dialog.T(req).send(500, 'Error while generating answer');
        }
    }

    PNG(req) {
        this.dialog.T(req).send(200, 'Pong');
    }

    BYE(req) {
        this.dbg(`Got BYE, closing dialog`);
        this.dialog.close();
        this.dialog.T(req).send(200, 'OK');
    }

    // FIXME: Setup a timer when delegating the transaction to a media
    // or datachannel server to ensure that we resolve the transaction
    // when something goes wrong in the handlers.
    RST(req) {
        this.dbg(`Got RST request`);

        const t = this.dialog.T(req);
        try {
            var body = JSON.parse(req.body);
        } catch(e) {
            t.send(400, 'Malformed RST Request');
            return;
        }

        if (typeof body.id === 'undefined') {
            t.send(400, 'Missing Stream ID');
            return;
        }

        let s;
        switch(body.type) {
            case 'media':
                s = this.connection.getStreamById(body.id);
                if (s == null) {
                    t.send(202, 'Stream Was Already Stopped');
                    return;
                }

                this.connection.removeStream(s);
                t.send(200, 'Stream Stopped');

                this.emit('rstMedia', this, req);
                return;
        }

        t.send(501, 'Not Implemented');
    }

    reject(req, T) {
        T(req).send(405, 'Method Not Allowed');
    }

    setup_dialog_callbacks(d) {
        let methods = ['SYN', 'RST', 'SDP', 'ICE', 'BYE', 'PNG'];

        methods.forEach(m => {
            this[m] = this[m].bind(this);
            d.on(m, this[m]);
        });

        this.reject = this.reject.bind(this);
        d.on(DEFAULT, this.reject);

        d.once('closed', () => {
            methods.forEach(m => {
                d.off(m, this[m]);
            });
            d.off(DEFAULT, this.reject);
            this.emit('disconnect', d);
        });
    }

    async sync(body) {
        let r = this.dialog.request(),
            t = this.dialog.T(r);
        this.dialog.add(t);
        r.method = 'SYN';
        r.body = JSON.stringify(body);

        t.start();

        let res = await t.resolved;
        if (!res.isSuccess()) {
            let e = new Error('Synchronization request failed');
            e.code = res.code;
            e.reason = res.reason;
            throw e;
        }

        return JSON.parse(res.body);
    }

    async sync_data(target, source) {
        let args = {
            id: this.dc_id,
            negotiated: true,
            ordered: true,
            reliable: true
        };
        this.dc_id += 2;

        const c = this.connection.createDataChannel('', args);

        try {
            await this.sync({
                type:    'data',
                target:  target,
                source:  source,
                options: dc2hdr(args)
            });
            return c;
        } catch(e) {
            c.close();
            throw e;
        }
    }

    async sync_media(target, source) {
        let body = await this.sync({
            type:   'media',
            target: target,
            source: source
        });

        return body.id;
    }
}


export class RTCPeer extends e2.EventEmitter2 {
    constructor(signaling_peer, rtc_config, opts) {
        super();

        this.rtc_config = rtc_config;

        if (opts) this.opts = {...opts};
        else this.opts = {};

        this.listening = false;
        this.signaling_peer = signaling_peer;
        this.signaling_peer.onAny((n,e) => this.emit(n, e));
        // FIXME: Uninstall the callback somewhere

        this.dbg = signaling_peer.dbg;

        this.mgr = new Manager();

        this.accept = this.accept.bind(this);
        this.reject = this.reject.bind(this);
    }

    get uri() {
        return this.signaling_peer.uri;
    }

    listen(cb) {
        this.dbg(`Listening for RTC dialog requests`);
        this.listening = true;
        this.cb = cb;
        this.signaling_peer.on('RTC',   this.accept);
        this.signaling_peer.on(DEFAULT, this.reject);
    }

    get listening() {
        return this._listening;
    }

    set listening(v) {
        if (this.listening != v) {
            this._listening = v;
            this.emit('listening', v);
        }
    }

    stopListening() {
        if (!this.listening) return;
        this.listening = false;
        delete this.cb;
        this.signaling_peer.off(DEFAULT, this.reject);
        this.signaling_peer.off('RTC',   this.accept);
    }

    reject(req, T) {
        T(req).send(405, 'Method Not Allowed');
    }

    async accept(req, T, D) {
        const uri = NodeURI.parse(req.from),
            t = T(req),
            rc = new RTCConnection(this, false, this.rtc_config, this.opts);

        void this.mgr.set(uri.node, rc);

        this.dbg(`Got RTC request from ${uri}`);
        t.send(100, 'Gathering ICE Candidates');

        if (typeof req.DataChannel !== 'undefined') {
            this.dbg(`Opening signaling data channel to ${uri}`);
            const cfg = hdr2dc(req.DataChannel);
            rc.channel = rc.connection.createDataChannel(cfg.label, {
                negotiated: true, ...cfg
            });
        }

        try {
            var sdp = await create_answer(rc.connection, req.body);
        } catch(e) {
            t.send(500, 'Could Not Create Answer');
            void rc.close();
            return;
        }

        const d = D(t, T, {serverSide: true}),
            msg = Response.fromRequest(req, 200, 'OK');
        msg.body = sdp;
        t.send(msg);

        void rc.open(d);
        if (this.cb) this.cb(rc);
    }

    async SYN_media(c, t, req) {
        if (!this.on_media_request) {
            t.send(501, 'Not Implemented');
            return;
        }

        try {
            var sel = JSON.parse(req.body);
        } catch(e) {
            t.send(400, 'Malformed Media Sync Request');
            return;
        }

        t.send(100, 'Trying');
        try {
            var s = await this.on_media_request(sel, c, t, req);
            if (!s) {
                let e = new Error('Stream not found');
                e.code = 404;
                e.reason = 'Stream Not Found';
                throw e;
            }

            if (s != true)
                c.connection.addStream(s);

            const res = Response.fromRequest(req, 200, "OK");
            res.body = media2hdr(s);
            t.send(res);
        } catch(e) {
        // FIXME: close stream s
            t.send(e.code || 500, e.reason || 'Internal Server Error');
            if (!e.code) console.error(e, e.stack);
            return;
        }
    }

    async SYN_data(c, t, req) {
        if (!this.on_data_request) {
            t.send(501, 'Not Implemented');
            return;
        }

        try {
            var sel = JSON.parse(req.body);
        } catch(e) {
            t.send(400, 'Malformed Media Sync Request');
            return;
        }

        t.send(100, 'Trying');
        try {
            sel.options = hdr2dc(sel.options);

            var dc = await this.on_data_request(sel, c, t, req);
            if (!dc) {
                let e = new Error('Stream not found');
                e.code = 404;
                e.reason = 'Failed to Create DataChannel';
                throw e;
            }

            let res = Response.fromRequest(req, 200, 'DataChannel Created');
            res.body = req.body;
            t.send(res);
        } catch(e) {
            t.send(e.code || 500, e.reason || 'Internal Server Error');
            if (!e.code) console.error(e, e.stack);
        }
    }

    async connect_data(uri) {
        let u = ResourceURI.parse(uri),
            rc = await this.connect(u.node);
        return await rc.sync_data(u.resource, 'ui');
    }

    async connect_media(uri) {
        let u = ResourceURI.parse(uri),
            rc = await this.connect(u.node);

        let id = await rc.sync_media(u.resource, 'ui'),
            stream = rc.connection.getStreamById(id);

        if (stream) return stream;

        // We've gotten the ID of the stream, but the corresponding
        // RTCPeerConnection object does not have the stream available
        // yet. Setup an 'onaddstream' callback and return a promise that
        // resolves once the stream has been added.

        return new Promise((resolve, reject) => {
            let timer;

            const cb = (e) => {
                if (e == null) {
                    this.dbg(`RTCPeerConnection hasn't produced stream ${id}`);
                    // Timeout
                    rc.off('onaddstream', cb);
                    reject();
                    return;
                }

                if (e.stream.id != id) return;
                rc.off('onaddstream', cb);
                clearTimeout(timer);
                resolve(e.stream);
            }

            timer = setTimeout(() => cb(null), 5000);
            rc.on('onaddstream', cb);
        });
    }

    async connect(remote) {
        try {
            var uri = NodeURI.parse(remote);
        } catch(e) {
            uri = remote;
        }

        let rc = this.mgr.get(uri.node);
        if (!rc) {
            rc = await this.initiate_connection(uri);
        } else {
        // FIXME: Reinit the connection if we timeout or get a negative
        // response.
            this.dbg(`Reusing existing RTC connection to ${uri}`);
        }

        await rc.established;
        return rc;
    }

    // As far as I can tell, there is no way to force the
    // RTCPeerConnection to signal support for SCTP data channels in the
    // initial SDP offer. Thus, we need to create a dummy data channel
    // so that the SDP will contain the corresponding m line. We can use
    // this connection later as a direct signaling connection between
    // the peers.
    //
    // When support for SCTP is signaling in the initial offer/answer
    // exchange, opening data channel connections later should not
    // trigger new offer/answer exchanges. If support for data channels
    // is not signalled at the beginning, the first data channel will
    // trigger an offer/answer exchange.
    //
    // Care must be taken with negotiated data channels. Those tend to
    // trigger offer/answer exchange from both peers simultaneously
    // which results in a state conflict in the peer connection.

    async initiate_connection(uri) {
        this.dbg(`Establishing new RTC connection to ${uri}`);
        const c = new RTCConnection(this, true, this.rtc_config),
            args = { id: 0, negotiated: true, ordered: true,
                reliable: true, protocol: 'signaling'};

        void this.mgr.set(uri.node, c);
        c.channel = c.connection.createDataChannel('peer', args);

        let sdp = await create_offer(c.connection),
            r = this.signaling_peer.createRequest(uri),
            t = this.signaling_peer.T(r),
            d = this.signaling_peer.D(t, this.signaling_peer.T);

        r.type = 'RTC';

        r.DataChannel = dc2hdr(args);
        r.body = sdp;

        this.dbg(`Sending RTC request to ${uri}`);
        d.add(t);
        t.start();

        void c.open(d);
        let res = await d.established;
        try {
            await c.connection.setRemoteDescription({type: 'answer', sdp: res.body});
        } catch(e) {
            console.error(e, e.stack);
            d.bye();
            throw e;
        }
        return c;
    }

    // Stop a stream
    async reset(dialog, type, id) {

        let r = dialog.request(),
            t = dialog.T(r);
        dialog.add(t);
        r.method = 'RST';

        if (typeof type !== "string") throw new Error("Invalid stream type");

        r.body = JSON.stringify({
            type: type,
            id:   id
        });

        t.start();

        let res = await t.resolved;
        if (!res.isSuccess()) {
            let e = new Error('Request to stop stream synchronization failed');
            e.response = res;
            throw e;
        }
        return res;
    }
}
