export default function abort(error: any, signame = 'SIGTERM') {
    if (error !== undefined)
        console.error('Aborting: ', error);
    process.kill(process.pid, signame);
}