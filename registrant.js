import e2                  from 'eventemitter2';
import {GUID}              from './guid';
import { REGISTER_RETRY_INTERVAL, REGISTER_EXPIRES} from './common';


const STATE_STOPPED       = 0,
    STATE_UNREGISTERED  = 1,
    STATE_REGISTERING   = 2,
    STATE_REGISTERED    = 3,
    STATE_REFRESHING    = 4,
    STATE_UNREGISTERING = 5;


function isRegistered(s) {
    return s == STATE_REGISTERED || s == STATE_REFRESHING;
}


function isRunning(s) {
    return s !== STATE_STOPPED;
}


/**
 * Maintain a permanent registration with the registrar for the peer's
 * URI. Attempts to re-register on errors. Emits events on succesful
 * registration and errors.
 */

// FIXME: Fast forward the queue if it contains multiple registration
// requests, just register the most recent one. Similarly for
// unregister.
//

export class Registrant extends e2.EventEmitter2 {
    constructor(peer, registrar_uri, opts) {
        super();
        this.uri = peer.uri;
        this.peer = peer;
        this.registrar_uri = registrar_uri;

        if (opts) this.opts = {...opts};
        else this.opts = {};

        this._state = STATE_STOPPED;
        this.session = GUID();
        this.seq = 1;

        this.on('newListener', (e, f) => {
            if (!this.running) return;
            if (e === 'registered'   && this.registered)  f();
            if (e === 'unregistered' && !this.registered) f();
        });

        this.dbg = peer.dbg;
    }

    get state() {
        return this._state;
    }

    transitionTo(to) {
        setTimeout(() => {
            let prev = this._state;
            this._state = this._transition(this.state, to);
            if (this._state == STATE_STOPPED && this._stopped) {
                this._stopped();
                delete this._stopped;
            }

            if (isRegistered(prev) && !isRegistered(this._state))
                this.emit('unregistered');
            if (!isRegistered(prev) && isRegistered(this._state))
                this.emit('registered');
        });
    }

    cancel_pending() {
        if (this.timer) {
            clearTimeout(this.timer);
            delete this.timer;
        }

        if (this.transaction) {
            this.transaction.stop();
            delete this.transaction;
        }
    }

    _transition(from, to) {
        switch(from) {
            case STATE_STOPPED:
                switch (to) {
                    case STATE_STOPPED:
                    case STATE_UNREGISTERED:
                        // The user has requested the registrant to be stopped. This
                        // may cancel the transaction promise, which will result in a
                        // transition request to STATE_UNREGISTERED, hence we need to
                        // handle that here and return STATE_STOPPED instead.
                        return STATE_STOPPED;

                    case STATE_REGISTERING:
                        this.dbg(`Starting registrant`);
                        void this.register();
                        return to;
                }
                break;

            case STATE_UNREGISTERED:
                switch(to) {
                    case STATE_STOPPED:
                        this.cancel_pending();
                        this.dbg(`Registrant stopped`);
                        return to;

                    case STATE_REGISTERING:
                        this.dbg(`Retrying registration`);
                        void this.register();
                        return to;
                }
                break;

            case STATE_REGISTERING:
                switch(to) {
                    case STATE_UNREGISTERING:
                        this.dbg(`Stopping registrant`);
                        void this.unregister();
                        return to;

                    case STATE_UNREGISTERED:
                        this.dbg(`Registration failed, retrying in ${REGISTER_RETRY_INTERVAL / 1000} seconds`);
                        this.cancel_pending();
                        this.timer = setTimeout(() => {
                            this.transitionTo(STATE_REGISTERING);
                        }, REGISTER_RETRY_INTERVAL);
                        return to;

                    case STATE_REGISTERED:
                        this.dbg(`Registered`);
                        if (this.expires) {
                            let e = parseInt(this.expires / 2);
                            this.dbg(`Refreshing after ${e} seconds`);
                            this.cancel_pending();
                            this.timer = setTimeout(() => {
                                this.transitionTo(STATE_REFRESHING);
                            }, e * 1000);
                        }
                        return to;
                }
                break;

            case STATE_REGISTERED:
                switch(to) {
                    case STATE_UNREGISTERING:
                        this.dbg(`Stopping registrant`);
                        void this.unregister();
                        return to;

                    case STATE_REFRESHING:
                        this.dbg(`Refreshing registration`);
                        void this.register();
                        return to;
                }
                break;

            case STATE_REFRESHING:
                switch(to) {
                    case STATE_UNREGISTERING:
                        this.dbg(`Stopping registrant`);
                        void this.unregister();
                        return to;

                    case STATE_UNREGISTERED:
                        this.dbg(`Refresh failed, retrying in ${REGISTER_RETRY_INTERVAL / 1000} seconds`);
                        this.cancel_pending();
                        this.timer = setTimeout(() => {
                            this.transitionTo(STATE_REGISTERING);
                        }, REGISTER_RETRY_INTERVAL);
                        return to;

                    case STATE_REGISTERED:
                        this.dbg(`Refresh succeeded`);
                        if (this.expires) {
                            let e = parseInt(this.expires / 2);
                            this.dbg(`Refreshing after ${e} seconds`);
                            this.cancel_pending();
                            this.timer = setTimeout(() => {
                                this.transitionTo(STATE_REFRESHING);
                            }, e * 1000);
                        }
                        return to;
                }
                break;

            case STATE_UNREGISTERING:
                switch(to) {
                    case STATE_STOPPED:
                        this.dbg(`Registrant stopped`);
                        return to;
                }
                break;
        }

        throw new Error(`Bug: Invalid registrant state transition: ${from} -> ${to}`);
    }

    get registered() {
        return isRegistered(this.state);
    }

    get running() {
        return isRunning(this.state);
    }

    start() {
        if (this.running) return;
        this.transitionTo(STATE_REGISTERING);
    }

    async stop() {
        if (!this.running) return;
        if (this.state == STATE_UNREGISTERED)
            this.transitionTo(STATE_STOPPED);
        else
            this.transitionTo(STATE_UNREGISTERING);

        await new Promise(resolve => this._stopped = resolve);
    }

    async register() {
        this.cancel_pending();
        try {
            let r = await this.make_request('REGISTER', REGISTER_EXPIRES);
            this.expires = parseInt(r.Expires);
            if (isNaN(this.expires)) delete this.expires;
            this.transitionTo(STATE_REGISTERED);
        } catch(e) {
            this.dbg(e);
            this.transitionTo(STATE_UNREGISTERED);
        }
    }

    async unregister() {
        this.cancel_pending();
        try {
            await this.make_request('UNREGISTER');
        } catch(e) {
            this.dbg(e);
        }
        this.transitionTo(STATE_STOPPED);
    }

    async make_request(method, expires) {
        let r = this.peer.createRequest(this.registrar_uri);
        r.method = method;

        if (expires) r.Expires = expires;

        r.Session = {
            sid: this.session,
            seq: this.seq++
        };

        if (this.opts.keychain) {
            let t = await this.opts.keychain.get('signaling');
            if (t) r.Authorization = {t: 'Bearer', v: t.id};
        }

        // If there is an existing transaction, stop it first
        if (this.transaction) await this.transaction.stop();

        this.transaction = this.peer.T(r);
        this.transaction.start();

        try {
            let res = await this.transaction.resolved;
            if (res.code < 200 || res.code >= 300) {
                let e = new Error(`${res.code} ${res.reason}`);
                e.response = res;
                throw e;
            }
            delete this.transaction;
            return res;
        } catch(e) {
            delete this.transaction;
            throw e;
        }
    }
}
