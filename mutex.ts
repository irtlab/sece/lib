import { EventEmitter } from 'events';

export default class Mutex {
    _locked: boolean;
    _emitter: EventEmitter;

    constructor() {
        this._locked = false;
        this._emitter = new EventEmitter();
    }

    acquire() {
        return new Promise<void>(resolve => {
            if (!this._locked) {
                this._locked = true;
                resolve();
                return;
            }

            const try_acquire = () => {
                if (!this._locked) {
                    this._locked = true;
                    this._emitter.removeListener('release', try_acquire);
                    resolve();
                }
            };

            this._emitter.on('release', try_acquire);
        });
    }

    release() {
        this._locked = false;
        setImmediate(() => this._emitter.emit('release'));
    }
}
