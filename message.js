import {PROTO_VERSION, TTL} from './common';

const COPY_HEADERS = ['Dialog', 'Via', 'Record-Route'];


const ContentParsers = {
    'text/plain'      : (c) => c,
    'application/json': (c) => JSON.parse(c)
};


/**
 * Generic signaling message. Each message is a JSON object and must have the
 * following properties:
 *   - v: Protocol version (currently 2)
 *   - t: Time to live
 *   - d: Destination URI
 *   - s: Source URI
 *
 * The following properties are optional:
 *   - m: Request/response method or code
 *   - b: Message body (payload)
 *   - c: Type of the message body (content type)
 */
export class Message {

    /**
     * Create a new signaling message
     *
     * @param {string} to   - Destination address
     * @param {string} from - Source address
     * @param {string} data - Message payload
     * @param {string} type - Message type
     * @return {string} A JSON message encoded as string
     */
    constructor(dst, src) {
        this.proto = PROTO_VERSION;
        this.ttl = TTL;
        this.to = dst;
        this.from = src;
    }

    /**
     * Convert a message to string. Ignore any properties whose names
     * start with '_'.
     */
    toString() {
        return JSON.stringify(this.toJSON());
    }

    /**
     * Produce a JSON serializable version of the message. This is the
     * main function called by the system before sending the message
     * over a channel, e.g., WebSocket, DataChannel, or API.
     */
    toJSON() {
        let rv = {};
        for (var key in this) {
            if (key.startsWith('_')) continue;

            let v = this[key];
            rv[key] = typeof v.toJSON === 'function' ? v.toJSON() : v;
        }
        return rv;
    }

    get version()  { return this['Proto']; }
    set version(v) { this['Proto'] = v; }

    get proto()  { return this['Protol']; }
    set proto(v) { this['Proto'] = v; }

    get ttl()  { return this['TTL']; }
    set ttl(v) { this['TTL'] = v; }

    get to()  { return this['To']; }
    set to(v) { this['To'] = v; }

    get dst()  { return this['To']; }
    set dst(v) { this['To'] = v; }

    get from()  { return this['From']; }
    set from(v) { this['From'] = v; }

    get src()  { return this['From']; }
    set src(v) { this['From'] = v; }

    get method()  { return this['Type']; }
    set method(v) { this['Type'] = v; }

    get code()  { return this['Type']; }
    set code(v) { this['Type'] = v; }

    get type()  { return this['Type']; }
    set type(v) { this['Type'] = v; }

    get dialog()  { return this['Dialog']; }
    set dialog(v) { this['Dialog'] = v; }

    get body()  { return this['Body']; }
    set body(v) { this['Body'] = v; }

    get bodyType()  { return this['Content-Type']; }
    set bodyType(v) { this['Content-Type'] = v; }

    get payload()  { return this['Body']; }
    set payload(v) { this['Body'] = v; }

    get payloadType()  { return this['Content-Type']; }
    set payloadType(v) { this['Content-Type'] = v; }

    get content()  { return this['Body']; }
    set content(v) { this['Body'] = v; }

    get contentType()  { return this['Content-Type']; }
    set contentType(v) { this['Content-Type'] = v; }

    get via()  { return this['Via']; }
    set via(v) { this['Via'] = v; }

    get route()  { return this['Route']; }
    set route(v) { this['Route'] = v; }

    // Return the content (body) parsed according to the content type.
    // Throws an error if the content type is unknown or unsupported.
    get parsedContent() {
        if (typeof this.content === 'undefined')
            return this.content;

        if (typeof this.contentType === 'undefined')
            throw new Error('Unknown content type');

        const parser = ContentParsers[this.contentType];
        if (typeof parser === 'undefined')
            throw new Error(`Unsupported content type "${this.contentType}"`);

        return parser(this.content, this);
    }

    /**
     * Parse a signaling message from string. Raise an Error on error.
     *
     * @param {string} data - The message to be parsed encoded in string
     * @return {Object} The message represented as an Object
     */
    static parse(data) {
        let msg;
        try {
            msg = JSON.parse(data);
        } catch(e) {
            let err = new Error(`Malformed signaling message`);
            err.message = data;
            throw err;
        }

        // Make sure the message contains exactly the protocol version that this
        // implementation supports.
        if (msg['Proto'] !== PROTO_VERSION)
            throw new Error(`Unsupported protocol version: ${msg['Proto']}`);

        // The TTL header must be present and it must be a number
        if (typeof msg['TTL'] !== 'number')
            throw new Error(`Invalid TTL value ${msg['TTL']}`);

        // If the method attribute is present, it must be a string, e.g.,
        // "REGISTER", or a number, e.g., 200.
        if (typeof msg['Type'] !== 'undefined') {
            if (typeof msg['Type'] !== 'string' && typeof msg['Type'] !== 'number')
                throw new Error(`Invalid message type ${msg['Type']}`);
        }

        let rv;
        if (typeof msg['Type'] === 'string') rv = new Request(msg['To'], msg['From']);
        else rv = new Response(msg['To'], msg['From']);

        for (var key in msg) {
            if (!Object.prototype.hasOwnProperty.call(msg, key)) continue;
            if (key.startsWith('_')) continue;
            if (key === 'To') continue;
            if (key === 'From') continue;
            rv[key] = msg[key];
        }

        return rv;
    }

    get isRequest() {
        return typeof this.type === 'string';
    }
}

export class Request extends Message {
    constructor(dst, src) {
        super(dst, src);
    }

    get isRequest() {
        return true;
    }

    // Return the next hop for the message. If we have routes, return
    // the first route, otherwise return the destination uri of the
    // message.
    get nextHop() {
        let r = this.route;
        if (Array.isArray(r) && r.length > 0) return r[0];
        return this.to;
    }

    addRoute(uri) {
        this.route = this.route || [];
        this.route.push(uri);
    }

    addRecordRoute(uri) {
        this['Record-Route'] = this['Record-Route'] || [];
        this['Record-Route'].unshift(uri);
    }

    // Consume the first route from the array if routes, if any.
    shiftRoute() {
        let r = this.route;
        if (Array.isArray(r) && r.length > 0) return r.shift();
        else return this.to;
    }

    addVia(uri, link, tid) {
        let v = {};
        this.via = this.via || [];

        v.uri = uri;
        if (typeof tid !== 'undefined') v.tid = tid;
        if (typeof link !== 'undefined') v.link = link;
        this.via.unshift(v);
    }
}

export class Response extends Message {
    static fromRequest(req, code, reason) {
        let res = new Response(req.to, req.from);
        res.type = code;
        if (reason) res['Reason'] = reason;
        res._copyHeaders(req);
        return res;
    }

    isSuccess() {
        return (this.code >= 200 && this.code < 300);
    }

    get isRequest() {
        return false;
    }

    shiftVia() {
        let v = this.via;
        if (Array.isArray(v) && v.length > 0) return v.shift();
        throw new Error('No more Via headers left');
    }

    _copyHeaders(req) {
        for(var h of COPY_HEADERS)
            if (req[h]) this[h] = req[h];
    }
}
