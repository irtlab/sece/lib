const ms  = 1;
const sec = 1000;
const min = 60 * sec;

export const REGISTER_RETRY_INTERVAL    = 60 * sec;
export const REGISTER_EXPIRES           = 1 * 60;
export const PROTO_VERSION              = 'SECE 1.0';
export const TTL                        = 8;
export const TRANSACTION_TIMEOUT        = 20 * sec;
export const TRANSACTION_DELETE_TIMEOUT = 30 * sec;
export const DIALOG_DELETE_TIMEOUT      = 1 * min;
export const RETRANSMIT_TIMEOUT_MIN     = 500 * ms;
export const RETRANSMIT_TIMEOUT_MAX     = RETRANSMIT_TIMEOUT_MIN * 4;
export const RETRANSMIT_PROVISIONAL     = 15 * sec;

// Ideally, these would be implemented as Symbols, but the
// eventemitter node library does not seem to support those.
export const ALL      = '_all';
export const DEFAULT  = '_default';
export const RESPONSE = '_response';
