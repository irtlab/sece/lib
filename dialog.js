import e2                      from 'eventemitter2';
import debug                   from 'debug';
import {GUID}                  from './guid';
import {DIALOG_DELETE_TIMEOUT} from './common';
import {Request, Response}     from './message';
import {DEFAULT, ALL}          from './common';

const dbg = debug('sece:D');

// The states of a dialog. A newly created dialog that hasn't been yet
// confirmed by the remote party is in DIALOG_NEW state. The dialog
// transitions to DIALOG_ESTABLISHED once it has been confirmed by the
// remote party. The dialog transitions to DIALOG_CLOSED if it could
// not be created or after it has been closed by either party.

const DIALOG_NEW         = 0,
    DIALOG_ESTABLISHED = 1,
    DIALOG_CLOSING     = 2,
    DIALOG_CLOSED      = 3;


export const DIALOG_STATE = {
    0: 'New',
    1: 'Established',
    2: 'Closing',
    3: 'Closed'
};

const MAX_SEQ = Math.pow(2, 32);


export class DialogManager extends e2.EventEmitter2 {
    constructor() {
        super();
        this.dialogs = {};
    }

    async stop() {
        dbg('Stopping dialog manager');
        await Promise.all(Object.values(this.dialogs).map(async (d) => {
            try {
                await d.bye();
            } catch(e) {
                d.dbg(e);
            }
        }));
        this.dialogs = {};
        dbg('Dialog manager stopped');
    }

    createDialog(transaction, T, opts, ...args) {
        let hdr = transaction.request.dialog ? {...transaction.request.dialog} : {},
            serverSide;

        // First weed out invalid Dialog parameter combinations
        if ((hdr.id && !hdr.src && hdr.dst) || (!hdr.id && (hdr.src || hdr.dst)))
            throw new Error('Bug: Invalid dialog header');

        // Second, check if we are attempting to create a new dialog for an
        // existing one by mistake
        if (hdr.id && hdr.src && hdr.dst)
            throw new Error('Bug: Attempt to create a new dialog with in-dialog request');

        if (!hdr.src) {
            hdr.id = GUID();
            serverSide = false;
        } else {
            hdr.dst = hdr.src;
            serverSide = true;
        }
        hdr.src = GUID();
        let id = hdr.src;
        if (serverSide) transaction.request.dialog.dst = hdr.src;

        if (this.dialogs[id]) throw new Error('Dialog with conflicting ID exists');

        const d = new Dialog(serverSide, hdr, transaction, T, opts, ...args);
        this.dialogs[id] = d;

        const cleanup = () => {
            d.dbg(`Starting delete timer`);
            setTimeout(() => {
                delete this.dialogs[id];
                d.dbg(`Dialog deleted`);
                this.emit('delete', d);
            }, DIALOG_DELETE_TIMEOUT);
        };

        d.established.then(() => {
            d.dbg(`Dialog opened`);
            this.emit('open', d);
            d.once('closed', cleanup);
        }, e => {
            cleanup();
            throw e;
        });

        this.emit('create', d);
        return d;
    }

    rx(m, peer) {
        let hdr = m.dialog || {},
            id = hdr.id;

        if (typeof id === 'undefined' || id === null)
            return false;

        // This is a request to establish a new dialog. Do not match the dialog
        // against existing ones. Let the peer emit an event where the subscriber
        // can create a new dialog.
        if (!hdr.dst) return false;

        let d = this.dialogs[hdr.dst];
        if (d) {
            // Matching dialog found. Pass the request to the dialog and indicate to
            // the peer that we took care of the request by returning true;
            d.rx(m, peer);
        } else {
            // Received an in-dialog request for which no dialog exists. Send an
            // error back and indicate to the peer that we took care of the request.
            peer.sendf(Response.fromRequest(m, 481, 'Dialog Does Not Exist'));
        }
        return true;
    }
}

// A dialog represents an ordered sequence of requests and responses sent from
// one peer to another. Each dialog object is a promise which resolves once
// the dialog has been succesfully created (confirmed by peer) and rejects if
// the peer rejected the dialog or could not be contacted.
//
// The purpose of a dialog is to ensure that a sequence of messages sent by
// the sender will be delivered by the recipient completely and with the same
// order. If that cannot be achieved, the dialog rejects within a given amount
// of time. That way, endpoints can always know whether the communication
// works reliably.
//

export class Dialog extends e2.EventEmitter2 {
    constructor(serverSide, hdr, transaction, T, opts) {
        super();
        this.hdr = {...hdr};
        this.id = hdr.src;
        this.seq = 1;
        this.state = DIALOG_NEW;
        this.T = T;
        this.opts = opts ? {...opts} : {};

        if (serverSide == true) {
            this.remote = transaction.request.from;
            this.local  = transaction.request.to;
        } else {
            this.remote = transaction.request.to;
            this.local  = transaction.request.from;
        }

        this.established = new Promise((resolve, reject) => {

            // Wait for the transaction that creates the dialog to finish. If the
            // transaction resolves succesfully with a 2xx then the remote peer
            // confirmed the dialog and the dialog is established. If the remote
            // peer returned some other response, or if the transaction failed to
            // resolve for some reason then the dialog was not created and we
            // immediately transition into DIALOG_CLOSED.

            transaction.resolved.then(res => {
                if (res.isSuccess()) {
                    if (!res.dialog.dst) {
                        let e = new Error('Missing dialog dst tag');
                        e.response = res;
                        throw e;
                    }
                    this.state = DIALOG_ESTABLISHED;
                    resolve(res);
                    if (!serverSide) this.hdr.dst = res.dialog.dst;
                } else {
                    this.close();
                    reject(res);
                }
            }, e => {
                this.close(e);
                reject(e);
            });
        });

        let tag = this.id.slice(0, 6);
        this.dbg = debug(`sece:D[${tag}]`);

        this.on('newListener', (e, f) => {
            switch(this.state) {
                case DIALOG_NEW:         if (e !== 'new') return; break;
                case DIALOG_CLOSED:      if (e !== 'closed') return; break;
                case DIALOG_CLOSING:     if (e !== 'closing') return; break;
                case DIALOG_ESTABLISHED: if (e !== 'established') return; break;
                default: return;
            }
            // eslint-disable-next-line @typescript-eslint/no-implied-eval
            setTimeout(f);
        });
    }

    get state() { return this._state; }

    set state(v) {
        if (this._state == v) return;
        this._state = v;
        setTimeout(() => this.emit('state', this, this.state));

        switch(this.state) {
            case DIALOG_NEW:         setTimeout(() => this.emit('new', this));         break;
            case DIALOG_CLOSED:      setTimeout(() => this.emit('closed', this));      break;
            case DIALOG_CLOSING:     setTimeout(() => this.emit('closing', this));     break;
            case DIALOG_ESTABLISHED: setTimeout(() => this.emit('established', this)); break;
        }
    }

    add(transaction) {
        transaction.request.dialog = {...this.hdr};
        transaction.request.dialog.seq = this.seq++ % MAX_SEQ;

        this.dbg(`New request, Seq: ${transaction.request.dialog.seq}`);
        transaction.resolved.then(res => {
            if (res.code == 481) {
                this.dbg('Dialog diappeared at remote end');
                this.close();
            }
        });
    }

    close() {
        this.dbg('Dialog closed');
        this.state = DIALOG_CLOSED;
    }

    bye() {
        switch(this.state) {
            case DIALOG_NEW:
                throw new Error('Invalid dialog state for BYE');
            case DIALOG_CLOSING:
            case DIALOG_CLOSED:
                return this._bye_resolved;
        }

        const msg = this.request(),
            t = this.T(msg);
        msg.method = 'BYE';
        this.add(t);

        this.dbg("Closing dialog");
        this.state = DIALOG_CLOSING;

        const cleanup = () => this.close();

        t.start();
        return t._bye_resolved = t.resolved.then(cleanup, e => {
            cleanup();
            throw e;
        });
    }

    request() {
        switch(this.state) {
            case DIALOG_CLOSING:
            case DIALOG_CLOSED:
                throw new Error("Can't create a request within closing or closed dialog");
        }

        let m = new Request(this.remote, this.local);
        m.addVia(this.local);

        return m;
    }

    rx(m, peer) {
        let hdr = m.dialog || {},
            id = hdr.dst ? hdr.dst.toString() : null;

        if (typeof id !== 'string')    throw new Error('Bug: Invalid or missing dialog id');
        if (hdr.create)                throw new Error('Bug: Request to create dialog matched to an existing dialog');
        if (id !== this.id.toString()) throw new Error('Bug: Dialog IDs do not match');

        let listeners =
        this.listenerCount(m.method)
        + this.listenerCount(DEFAULT)
        + this.listenerCount(ALL);

        // Check whether anything is listening on the dialog. A dialog without any
        // listeners should not happen, at least BYE requests must be handled by
        // the upper layers.
        if (listeners == 0) throw new Error('Bug: Dialog without listeners');

        this.emit(m.method, m, peer.T, peer.D, peer.sendf);
        if (this.listenerCount(m.method) == 0)
            this.emit(DEFAULT, m, peer.T, peer.D, peer.sendf);
        this.emit(ALL, m, peer.T, peer.D, peer.sendf);
    }
}
